//*************************************************
// jsLoader.js
// (c)pascal TOLEDO http://www.legral.fr
// date 20 fevrier 2010
// v1.0: 
//	chargeur de librairie (statique)
// v1.1:
//	Mise en objet (pas de verification de bon chargement du lien)
// *************************************************

TjsLoader_lien = function (init,options)
	{
	// obligatoires
	this.nom=		init.nom;
	this.mime=	init.mime;
	this.typeAppelle= init.typeAppelle;	//'statique', 'spip'
	this.mime=	init.mime;	// 'js', 'css'

	// optionnelles
	this.media=	'all';	//css: media de destination

	if (options != undefined)
		{	
		if (options.media != undefined) {this.media= options.media;}
		}
	//gestion d'etat et function
	this.loaded=	0;
	this.setLoaded= function () {this.loaded= 1;}
	this.setTypeAppelle= function (ta) {this.typeAppelle= ta;}
	}


TjsLoader= function()
	{
	this.nb = 0;
	this.liens= Array();
	this.isLoaded= function (lien)
		{return( (this.liens[lien] != undefined) && (this.liens[lien].loaded === 1 ));
		}

	// fonction d'ajout de lien
	this.addStatique= function(mime, lien, options)
		{
		if (!this.isLoaded(lien))
			{
			this.liens[lien] = new TjsLoader_lien({mime: mime, typeAppelle: 'statique',nom:lien},options);	// creation de l'objet lien

			switch (mime)
				{
				case 'js':document.write('<script type="text/javascript" src="'+lien+'"></script>');break;
				case 'css':
					document.write('<link rel="stylesheet" href="' +lien+ '"type="text/css"media="' +this.liens[lien].media+ '" />');
					break;
				}	// switch		
			this.nb++;
			this.liens[lien].setLoaded();
			}
		}	// this.addStatique()

	this.addSPIP=  function (mime, id_article, options)
		{
		if (!this.isLoaded(id_article))
			{
			this.liens[id_article] = new TjsLoader_lien({mime: mime, typeAppelle: 'spip',nom: id_article},options);
			switch (mime)
				{
				case 'js':
					var test_getCode = new Ajax.Request('/spip.php',
						{
						requestHeaders: ['Accept','text/javascript'],
						method: 'get',
						parameters:	{page:'cgp-getCode',id_article: id_article}
						// onFailure: function(requester){errNu=1;},
						// onSuccess: function(requester){errNu=0;}	//lib charger correctement dans l'env javascript (des erreurs d'execution restent possibles)
						});
						// injection du code dans la memoire et evaluation automatique
						break;
				case 'css':
					// traiter le code CSS // non IMPLEMENTER
					break;

				}	// switch		
			this.nb++;
			this.liens[id_article].setLoaded();

			//debug(' - responseText:'+test_getCode.transport.responseText);
			//test_getCode.evalResponse(); //si ajout de php header impossible (header('Content-type: text/javascript');)!!!
			return (this.liens[ id_article].isLoaded);
			}
		}	// this.addSPIP


	this.add= function (mime, typeAppelle, lien,options)	
		{
		if (this.isLoaded(lien))
			{return(0);
			}
			{
			switch (typeAppelle)
				{
				case 'statique':
					this.addStatique(mime, lien, options);
					break;
				case 'spip':
					this.addSPIP(mime, lien, options);		// contient l'etat (erreur ou non) et non le code lui meme (memoire js)
					break;
				//		default: debug('Type non compatible');
				}
			}
		}
	};	// TjsLoader= function()
	
jsLoader= new TjsLoader();
